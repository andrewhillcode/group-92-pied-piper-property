<div class="container">
  <div class="page-header">
    <h1 class="text-center">About us</h1>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-sm-8">
        <p>Pied Piper Property is the result of hard word and dedication from the founder, Mr. David. For 20 years he has managed his own real estate company through websites like Gumtree. His attention to detail and perseverance is key to the success of his business Pied Piper Property.

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sed elit eu purus vestibulum imperdiet at nec magna. Nunc mauris erat, scelerisque at sapien ac, laoreet sollicitudin nibh. Quisque tristique lobortis felis, id malesuada augue lacinia sit amet. Mauris tellus leo, scelerisque at sem quis, tempor tristique nulla. Duis mollis diam sed viverra elementum. Quisque vulputate scelerisque ante ac mattis. Vestibulum feugiat arcu felis, nec porttitor lorem suscipit quis. Curabitur facilisis, tortor sed egestas blandit, arcu libero consectetur nulla, id ultrices nunc orci nec ex. Quisque fringilla porttitor turpis sed hendrerit. In eget porta est.</p>

        <p>In quam tellus, pellentesque sit amet euismod sed, pretium vel augue. Aenean a lacus sagittis, sodales ligula et, dictum enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ac mattis metus. Curabitur at lorem condimentum, elementum lacus et, pellentesque dui. In facilisis, magna vitae iaculis efficitur, nulla leo molestie tortor, quis tincidunt sem sem eget turpis. In consequat nibh eu gravida efficitur. Aenean molestie fermentum augue eget posuere. Mauris dui lorem, ultrices vitae velit non, molestie mollis dui. Quisque eu erat sit amet sapien dapibus efficitur. In hac habitasse platea dictumst. Ut bibendum nibh vel metus sollicitudin finibus. Morbi laoreet quis ante vitae aliquam. Suspendisse consectetur tempor arcu in commodo. Integer ut quam in erat venenatis pharetra non vel est. </p>

        <p>Proin tempor lorem eget erat euismod, sed ullamcorper felis vulputate. Suspendisse at sapien interdum, euismod tortor eu, varius est. Donec lorem elit, elementum quis accumsan in, ultricies vel velit. Donec porttitor, massa a mollis aliquet, ante tellus tempus ipsum, vitae accumsan turpis nulla sit amet ligula. Aliquam nisi massa, cursus et volutpat a, pellentesque quis neque. Nunc mauris nunc, mattis nec semper nec, suscipit sit amet odio. Aenean fermentum libero commodo auctor porta. Donec varius augue nec porta eleifend. Nullam et convallis urna. Etiam consectetur tincidunt velit, et tempor erat.</p>
      </div>
      <div class="col-sm-4">
        <img src="img/david.png" alt="House 1" class="img-thumbnail" >
      </div>
    </div>
  </div>
  <div class="container">
    <div class="page-header">
      <h1 class="text-center">Contact</h1>
    </div>
  </div>
  <div class="container">
    <div class="col-sm-12 contact-details">
      <p>contactus@piedpiper.com.au</p>
      <p>Ph: 3333 3333</p>
      <p>Mob: 04 1234 5678</p>
    </div>
  </div>
</div>