<?php
include_once'inc/loginsession.inc';
include 'inc/PDO.inc';

//Persistent login info, change displays if user is logged in / not logged in
  if (isset($_GET['logout'])) {
    if (isset($_SESSION['email'])) {
      unset($_SESSION['email']);
    }
  }
?>

<!-- Register Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Register</h4>
      </div>
      <div class="account-wall">
        <img class="profile-img" src="img/footer-logo.png" alt="">
        <form method="POST" class="form-signin">
          <input type="text" name="fname" class="form-control" placeholder="First Name" required autofocus>
          <input type="text" name="lname" class="form-control" placeholder="Last Name" required>
          <input type="text" name="email" class="form-control" placeholder="Email" required>
          <input type="text" name="phone" class="form-control" placeholder="Contact Number" required>
          <input type="password" name="password" class="form-control" placeholder="Password" required>
          <input type="password" class="form-control" placeholder="Confirm password" required>
          <br>
          <button class="btn btn-lg btn-primary btn-block" name="register" type="submit">
            Register</button>
          <span class="clearfix"></span>
        </form>
        <a id="haveLoginButton" href="#" class="text-center new-account">Already have an account?</a>
      </div>
    </div>
  </div>
</div>
<?php
  if (isset($_POST['register'])) {
      $date = date("Y-m-d");

      try{
    $stmt = $pdo->prepare('INSERT INTO user_information (email, first_name, last_name, contact_number, password, date_joined) VALUES (:email, :fname, :lname, :phone, :password, :date)');
    $stmt->bindValue(':email', $_POST['email']);
    $stmt->bindValue(':fname', $_POST['fname']);
    $stmt->bindValue(':lname', $_POST['lname']);
    $stmt->bindValue(':phone', $_POST['phone']);
    $stmt->bindValue(':date', $date);
    $stmt->bindValue(':password', $_POST['password']);
    $stmt->execute();

     $stmt = $pdo->prepare('INSERT INTO tenants (email) VALUES (:email)');
    $stmt->bindValue(':email', $_POST['email']);
      $stmt->execute();

  }        catch (PDOException $e) {
          echo $e->getMessage();
        }
  }
?>
