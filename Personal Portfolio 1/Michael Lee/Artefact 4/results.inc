<?php
  include 'inc/PDO.inc';
  try {
    if ($_POST['greaterthan'] == 0) {
      $result = $pdo->prepare('SELECT * FROM house_information WHERE price <= :price AND address LIKE :search AND bed_number LIKE :beds AND bath_number LIKE :baths AND carport_number LIKE :cars AND furnished LIKE :furnished');
    } else {
      $result = $pdo->prepare('SELECT * FROM house_information WHERE price >= :price AND address LIKE :search AND bed_number LIKE :beds AND bath_number LIKE :baths AND carport_number LIKE :cars AND furnished LIKE :furnished');
    }
    $result->bindValue(':price', $_POST['price']);
    $result->bindValue(':search','%'.$_POST['search-field'].'%');
    $result->bindValue(':beds', '%'.$_POST['beds'].'%');
    $result->bindValue(':baths', '%'.$_POST['baths'].'%');
    $result->bindValue(':cars', '%'.$_POST['cars'].'%');
    $result->bindValue(':furnished', '%'.$_POST['furnished'].'%');
    $result->execute();
  }
  catch (PDOException $e) {
    echo $e->getMessage();
  }
?>

<div class="container">
  <div class="page-header">
    <h1 class="text-center">Results</h1>
  </div>
  <div class="container">
    <?php
      if ($result->rowCount() == 0) {
        echo "<h2>no results</h2>";
      } else {
        foreach($result as $house){
          try {
            $imagequery = $pdo->prepare('SELECT image_path FROM house_images WHERE house_id = :house_id ');
            $imagequery->bindValue(':house_id', $house['house_id']);
            $imagequery->execute();
          }
          catch (PDOException $e) {
            echo $e->getMessage();
          }
          $image = $imagequery->fetch();
          $image = $image[0];

          echo "<div class='row'>";
            echo "<div class='col-md-6 property-details'>";
              echo "<div>";
                echo "<h2>",$house['address'],"</h2>";
                echo "<img src='img/bed.png' alt='Beds'>";
                echo "<span class='property-icon'>",$house['bed_number'],"</span>";
                echo "<img src='img/bath.png' alt='baths'>";
                echo "<span class='property-icon'>",$house['bath_number'],"</span>";
                echo "<img src='img/car.png' alt='cars'>";
                echo "<span class='property-icon'>",$house['carport_number'],"</span>";
                echo "<h3>$",$house['price']," per week</h3>";
                echo "<a href='property.php?house_id=",$house['house_id'],"' class='btn btn-primary' title='See more'>More Details</a>";
              echo "</div>";
            echo "</div>";
            echo "<div class='col-md-6'>";
              echo "<img src='img/$image' alt='House' class='img-thumbnail'>";
            echo "</div>";
          echo "</div>";
        }
      }
    ?>
  </div>
</div>

<div class="container">
  <div class="page-header">
    <h1 class="text-center">Not what you were looking for?</h1>
  </div>
  <div class="col-lg-12  search-again">
    <a href="index.php" class="btn btn-primary" title="See more">Search again</a>  </div>
    <br><br><br><br>
<!-- /container -->
</div>
