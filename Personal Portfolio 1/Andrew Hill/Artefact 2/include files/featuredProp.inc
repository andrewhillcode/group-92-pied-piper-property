<div class="container">
  <div class="page-header">
    <h1 class="text-center">Featured Properties</h1>
  </div>
  <div class="col-lg-12 carousel-col">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="img/house1.jpg" alt="House 1" >
          <div class="carousel-caption">
            <h2>Wynnum</h2>
            <p>123 Test Road, Wynnum, Qld, 4178</p>
              <p>$506,000</p>
              <a href="#" class="btn btn-primary" title="See more">More Details</a>
          </div>
        </div>
        <div class="item">
          <img src="img/house2.jpg" alt="House 2" >
            <div class="carousel-caption">
              <h2>Manly</h2>
              <p>22 Test Road, Manly, Qld, 1111</p>
              <p>$600,000</p>
              <a href="#" class="btn btn-primary" title="See more">More Details</a>
          </div>
        </div>
        <div class="item">
          <img src="img/house3.jpg" alt="House 3" >
            <div class="carousel-caption">
              <h2>Redcliffe</h2>
              <p>72 Test Lane, Redcliffe, Qld, 4444</p>
              <p>$800,000</p>
              <a href="#" class="btn btn-primary" title="See more">More Details</a>
          </div>
        </div>
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</div>