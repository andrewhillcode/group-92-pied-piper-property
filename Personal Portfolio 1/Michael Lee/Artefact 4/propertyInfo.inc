<?php
  include 'inc/PDO.inc';

  $id = $_GET['house_id'];

  try {
    $query = $pdo->prepare('SELECT * '.
     'FROM house_information '.
     'WHERE house_id = :house_id ');
    $query->bindValue(':house_id', $id);
    $query->execute();
  }
  catch (PDOException $e) {
    echo $e->getMessage();
  }

  try {
    $images = $pdo->prepare('SELECT image_path '.
     'FROM house_images '.
     'WHERE house_id = :house_id ');
    $images->bindValue(':house_id', $id);
    $images->execute();
  }
  catch (PDOException $e) {
    echo $e->getMessage();
  }

  $imagenumber = $images->rowCount();

?>

<div class="container content-padding">
  <div>
    <div class="col-lg-4 text-center">
      <?php
        foreach($query as $detail) {
          echo "<h1>",$detail['address'],"</h1>";
          echo "<img src='img/bed.png' alt='Beds'>";
          echo "<span class='property-details'>",$detail['bed_number'],"</span>";
          echo "<img src='img/bath.png' alt='baths'>";
          echo "<span class='property-details'>",$detail['bath_number'],"</span>";
          echo "<img src='img/car.png' alt='cars'>";
          echo "<span class='property-details'>",$detail['carport_number'],"</span>";
          echo "<br><br>";
          echo "<p class='text-left'>",$detail['house_description'],"</p>";
        }
      ?>
    </div>
    <div class="col-lg-8 carousel-col">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
        <?php
          for ($i = 0; $i < $imagenumber; $i++){
            if ($i == 0) {
              echo "<li data-target='#carousel-example-generic' data-slide-to='",$i,"' class='active'></li>";
            } else {
              echo "<li data-target='#carousel-example-generic' data-slide-to='",$i,"'></li>";
            }
          }
        echo "</ol>";

        echo "<div class='carousel-inner' role='listbox'>";

        //$curr = $image[0];
        $image = $images->fetch();
        for ($i = 0; $i < $imagenumber; $i++) {
          $curr = $image[0];
          if ($i == 0) {
            echo "<div class='item active'>";
          } else {
            echo "<div class='item'>";
          }
          echo "<img src='img/$curr' alt='House'>";
          echo "</div>";
          $image = $images->fetch();
        }
        echo "</div>";
     ?>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="container content-padding">
  <div class='col-lg-4'>
  <?php
    echo "<h3>Rental Details</h3>";
    echo "<hr class='separator'>";
    echo "<span>Price:</span><h3>$",$detail['price'],"/week</h3>";
    if ($detail['furnished'] ==  1){
      $furnishing = 'Yes';
    } else {
      $furnishing = 'No';
    }
    echo "<span>Furnished?</span><h3>",$furnishing,"</h3>";
    echo "<span>Property Type:</span><h3>placeholder</h3>";
  ?>
  </div>
  <div class="col-lg-4">
    <h3>Property Viewing Times</h3>
    <hr class="separator">
    <h4>Monday 2pm</h4><a href="#" class="btn btn-primary inline-button" title="Book now">Book now</a>
    <br><br>
    <h4>Saturday 4pm</h4><a href="#" class="btn btn-primary inline-button" title="Book now">Book now</a>
    <br><br>
    <p>Property viewings are 20 minutes in duration and must be booked a day prior to the viewing.</p>
  </div>
   <div class="col-lg-4">
    <div class="map-placeholder">map placeholder</div>
  </div>
</div>
