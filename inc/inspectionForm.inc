<div class="container border schedule">
	  <h3 class="text-center">Schedule new Inspection</h3>
	  
  <form id="inspectionForm" action="inspections.php" onsubmit="return validateForm()" method="post">
    <select id='houseID' required name="houseID" >
		<option value=''>Select House</option>
		<?php
			foreach($houses as $house)
			{
			echo '<option value=',$house['house_id'],'>',$house['address'],'</option>';
			}
		?>
		</select>
		
	Date: <input id='date' name='date' type="date" required placeholder='dd/mm/yyyy'></input>
	Completed: <input name='complete' type="checkbox" value=1></input><br>
	<textarea id="details" name="details" form="inspectionForm" placeholder="Extra Details"></textarea>
	<input type="hidden" name="updated" value="true"></input>
	<input id='inspectionID' type="hidden" name="inspectionID" value=""></input>
	<input type="submit" id='createButton' class="btn createButton btn-primary" name="action" value="Create">
	<input type="submit" id='updateButton' class="btn hidden btn-primary" name="action" value="Update">
	<input type="submit" id='deleteButton' class="btn hidden btn-danger" name="action" value="Delete">
	</form>
	
</div>