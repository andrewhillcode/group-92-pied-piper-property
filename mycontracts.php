<head>
	<title>Contracts</title>
  <link rel="icon" type="image/png" href="img/favicon.png" />
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<!-- override stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/defaultStyle.css">
  <!-- results stylesheet -->
  <link rel="stylesheet" type="text/css" href="css/myContractsStyle.css">
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>

<?php
error_reporting(-1);
ini_set('display_errors', 'On');
	include 'inc/PDO.inc';
	include 'inc/navigation.inc';
	?>

<?php
  include 'inc/PDO.inc';
 


  //find tenant id
  try {
    $getID = $pdo->prepare('SELECT tenant_id FROM tenants WHERE email = :email');
    $getID->bindValue(':email', $_SESSION['email']);
    $getID->execute();
  }
  catch (PDOException $e) {
    echo $e->getMessage();
  }


  $tenantID = $getID->fetch();
  $tenantID = $tenantID[0];

  try {
    $result = $pdo->prepare('SELECT houses.house_id, address, price, owners.email as owner, staff.email as agent '.
	'FROM house_information as houseInfo INNER JOIN renting_tenants as tenants ON houseInfo.house_id = tenants.house_id '.
	'INNER JOIN houses ON houseInfo.house_id = houses.house_id '.
	'INNER JOIN owners ON owners.owner_id = houses.owner_id '.
	'INNER JOIN staff_members AS staff ON houses.staff_id = staff.staff_id '.
	'WHERE tenant_id = :tenant_id;');
    $result->bindValue(':tenant_id', $tenantID);
    $result->execute();
  }
  catch (PDOException $e) {
    echo $e->getMessage();
  }
?>

<div class="container">
  <div class="page-header">
    <h1 class="text-center">My Contracts</h1>
  </div>
  <div class="container">
    <?php
      $i = 0;
      if ($result->rowCount() == 0) {
        echo "<h2>You aren't renting any properties</h2>";
      } else {

        echo "<div class='row'>";
        echo "<div class='col-md-10'>";

        echo "<table class='property-table'>";
          echo "<tr>";
            echo "<td></td>";
            echo "<td><b>Address</b></td>";
            echo "<td>Rent</td>";
			echo "<td>Agent Email</td>";
			echo "<td>Owner Email</td>";
          echo "</tr>";
        foreach($result as $house) {       
              echo "<tr>";
			  echo "<td></td>";
                echo "<td>",$house['address'],"</td>";
				echo "<td>$",$house['price'],"/Month</td>";
				echo "<td>",$house['agent'],"</td>";
				echo "<td>",$house['owner'],"</td>";
                echo "<td class='buttonCol'><a href='property.php?house_id=",$house['house_id'],"' class='btn btn-primary' title='Details'>Details</a></td>";
              echo "</tr>";
            }
          echo "</table>";
        echo "</div>";
      echo "</div>";
        
      }
    ?>
  </div>
</div>

	<?php
	include 'inc/footer.inc';
?>

<script src="js/headerScript.js"></script>
<script src="js/modalScript.js"></script>
