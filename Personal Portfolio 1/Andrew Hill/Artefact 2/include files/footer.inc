<div class="footer">
  <div class="container">
    <div class="col-sm-4">
      <img src="img/logo-small.png">
    </div>
    <div class="col-sm-4">
        <h3>Navigation</h3>
        <hr>
        <ul>
          <li>Home</li>
          <li>Listings</li>
          <li>About Us</li>
          <li>Register</li>
          <li>Contact</li>
        </ul>
    </div>
    <div class="col-sm-4">
       <h3>About Team 92</h3>
        <hr>
        <span><b>Members</b></span>
        <br>
        <ul>
          <li>Andrew Hill</li>
          <li>Michael Lee</li>
          <li>Rohan Schemioneck</li>
        </ul>
        <ul>
          <li>Christian Kissane</li>
          <li>Alan Hili</li>
          <li>Alex Holder</li>
        </ul>
        <br>
      <span>Website created for IFB299 @ Queensland University of Technology</span>
    </div>
  </div>
</div>