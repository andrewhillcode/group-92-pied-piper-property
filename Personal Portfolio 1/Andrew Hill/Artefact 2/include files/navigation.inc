<div id="bg-img" class="background-image"> </div>

<nav id="navbar" class="navbar navbar-default navbar-static-top">
	<div class="container">
		 <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img alt="Brand" src="img/pp.png" href="index.php">
      </a>
			<div class="nav navbar-nav navbar-left">
				<p class="navbar-text">Pied Piper Property</p>
	    </div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
	      <li><a href="index.php">Home</a></li>
	      <li><a href="search.php">Listings</a></li>
		    <li><a href="about.php">About Us</a></li>
        <li><a id="loginButton" href="#">Login</a></li>
	      <li><a id="registerButton" href="#">Register</a></li>
	    </ul>
		</div>
  </div>
</nav>

<!-- Login Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Login</h4>
      </div>
      <div class="account-wall">
        <img class="profile-img" src="img/footer-logo.png" alt="">
        <form action="index.html" class="form-signin">
          <input type="text" class="form-control" placeholder="Email" required autofocus>
          <input type="password" class="form-control" placeholder="Password" required>
          <button class="btn btn-lg btn-primary btn-block" type="submit">
            Login</button>
          <span class="clearfix"></span>
        </form>
        <a id="noAccountButton" href="#" class="text-center new-account">Don't have an account?</a>
      </div>
    </div>
  </div>
</div>

<!-- Register Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Register</h4>
      </div>
      <div class="account-wall">
        <img class="profile-img" src="img/footer-logo.png" alt="">
        <form action="index.html" class="form-signin">
          <input type="text" class="form-control" placeholder="Name" required autofocus>  
          <input type="text" class="form-control" placeholder="Email" required>
          <input type="password" class="form-control" placeholder="Password" required>
          <input type="password" class="form-control" placeholder="Confirm password" required>
          <br>
          <button class="btn btn-lg btn-primary btn-block" type="submit">
            Register</button>
          <span class="clearfix"></span>
        </form>
        <a id="haveLoginButton" href="#" class="text-center new-account">Already have an account?</a>
      </div>
    </div>
  </div>
</div>