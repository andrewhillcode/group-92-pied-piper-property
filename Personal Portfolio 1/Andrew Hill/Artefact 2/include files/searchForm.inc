<div id="jumbo" class="jumbotron container-fluid">
	<div class="container">
    <h1>Search our properties</h1>
      <form action="search.php" class="bs-example bs-example-form" role="form">
        <div class="row">
          <div class="col-lg-12">
            <div class="input-group big-input">
              <input type="text" name="search-field" class="form-control" placeholder="Enter an address, suburb, or postcode">
              <span class="input-group-btn">
                <input class="btn btn-default" value="Submit" type="submit">Search!</input>
              </span>
            </div><!-- /input-group -->
          </div><!-- /.col-lg-6 -->
        </div><!-- /.row -->
        <br>
        <fieldset>
          <legend>Refine search</legend>
          <div class="row">
            <div class="col-lg-3 col-centered">
              <div class="input-group right-aligned"> 
                <select name="greaterthan" class="form-control">
                  <option value="1">Greater Than</option>
                  <option value="0">Less Than</option>
                </select>
              </div>
           </div>
            <div class="col-lg-3 col-centered">
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" name="price" class="form-control" placeholder="Price per week">
              </div><!-- /input-group -->
            </div>
          <div class="row">
            <div class="col-lg-3 col-centered">
              <div class="input-group"> 
                <span class="input-group-addon">Type</span>
                <select name="type" class="form-control">
                  <option value="">All property types</option>
                  <option value="1">House</option>
                  <option value="2">Apartment</option>
                  <option value="3">Townhouse</option>
                </select>
              </div>
            </div>
            <div class="col-lg-2 col-centered">
              <div class="input-group"> 
                <span class="input-group-addon">Beds</span>
                <select name="beds" class="form-control">
                  <option value="">Any</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6+</option>
                </select>
              </div>
            </div>
            <div class="col-lg-2 col-centered">
              <div class="input-group"> 
                <span class="input-group-addon">Baths</span>
                <select name="baths" class="form-control">
                  <option value="">Any</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6+</option>
                </select>
              </div>
            </div>
            <div class="col-lg-2 col-centered">
              <div class="input-group"> 
                <span class="input-group-addon">Carports</span>
                <select name="cars" class="form-control">
                  <option value="">Any</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6+</option>
                </select>
              </div>
            </div>
            <div class="col-lg-2 col-centered">
              <div class="input-group"> 
                <span class="input-group-addon">Furnished</span>
                <select name="furnished" class="form-control">
                  <option value="">Any</option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
              </div>
            </div> 
          </div>
        </fieldset>
      </form> 
    </div>
  </div>
</div>