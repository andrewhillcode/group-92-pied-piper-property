<?php
$pdo = new PDO('mysql:host=localhost;dbname=ifb299', 'piperuser', 'password');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$field = $_GET['search-field'];
$greaterthan = $_GET['greaterthan'];
$type = $_GET['type'];
$beds = $_GET['beds'];
$baths = $_GET['baths'];
$cars = $_GET['cars'];
$furnished = $_GET['furnished'];
$price = $_GET['price']; 

try {
  $query = $pdo->prepare('SELECT * '.
   'FROM house_information '.
   'WHERE price = :price ');
  $query->bindValue(':price', $price);
  $query->execute();
}
catch (PDOException $e) {
  echo $e->getMessage();
}
?>

<div class="container">
  <div class="page-header">
    <h1 class="text-center">Results</h1>
  </div>
  <div class="container">
    <?php 
      if ($query->rowCount() == 0){
        echo "<h2>no results</h2>";
      } else {
        foreach ($query as $property){     
          try {
            $imagequery = $pdo->prepare('SELECT imagepath '.
             'FROM house_images '.
             'WHERE house_id = :house_id ');
            $imagequery->bindValue(':house_id', $property['house_id']);
            $imagequery->execute();
          }
          catch (PDOException $e) {
            echo $e->getMessage();
          }

          $image = $imagequery->fetch();
          $image = $image[0];

          echo "<div class='row'>";
            echo "<div class='col-md-6 property-details'>";
              echo "<div>";
                echo "<h2>",$property['address'],"</h2>";
                echo "<img src='img/bed.png' alt='Beds'>"; 
                echo "<span class='property-icon'>",$property['bed_number'],"</span>";
                echo "<img src='img/bath.png' alt='baths'>"; 
                echo "<span class='property-icon'>",$property['bath_number'],"</span>";
                echo "<img src='img/car.png' alt='cars'>"; 
                echo "<span class='property-icon'>",$property['carport_number'],"</span>";
                echo "<h3>$",$property['price']," per week</h3>";
                echo "<a href='property.php?house_id=",$property['house_id'],"' class='btn btn-primary' title='See more'>More Details</a>";
              echo "</div>";
            echo "</div>";
            echo "<div class='col-md-6'>";
              echo "<img src='img/",$image,".png' alt='House' class='img-thumbnail'>";
            echo "</div>"; 
          echo "</div>";
        }
      }
    ?>
  </div>
</div>

<div class="container">
  <div class="page-header">
    <h1 class="text-center">Not what you were looking for?</h1>
  </div>
  <div class="col-lg-12  search-again">
    <a href="index.php" class="btn btn-primary" title="See more">Search again</a>  </div>
    <br><br><br><br>
<!-- /container -->
</div>